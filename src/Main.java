
public class Main {

	public static void main(String[] args) {
		Library libr = new Library();
		Student stu1 = new Student("Thanawat Boonsri","5610451086","D14");
		Student stu2 = new Student("Ruttanagorn Boolun","5610450314","D14");
		//Professor tea = new Professor("David De Gea","D14") ;
		//Staff sta = new Staff("Somnum Naming","Beggar");// ����¹��� ��ҵ�� �ǹ�չ
		
		Book book1 = new Book("Marketing","2012");//����¹����
		Book book2 = new Book("PPL","2014");
		Book book3 = new Book("108 Question","2002");
		Book book4 = new Book("Software Analysis and Design","2010");
		Book book5 = new Book("Data Structure","2005");
		Book book6 = new Book("Korean Language","2011");
		
		RefBook refBook1 = new RefBook("Dictionary","2010");
		RefBook refBook2 = new RefBook("Eclipse","2015");
		RefBook refBook3= new RefBook("KomChadLuek","2015");
		RefBook refBook4 = new RefBook("Java Reference","2012");
		RefBook refBook5 = new RefBook("Python Reference","2010");
		RefBook refBook6 = new RefBook("C++ Reference","2011");
		
		
		//add Book
		libr.addBook("Marketing", book1);
		libr.addBook("PPL",book2);
		libr.addBook("108 Question",book3);
		libr.addBook("Software Analysis and Design",book4);
		libr.addBook("Data Structure",book5);
		libr.addBook("Korean Language",book6);
		
		//add Reference book (Reference book can't be borrowed)
		libr.addRefBook("Dictionary",refBook1);
		libr.addRefBook("Eclipse",refBook2);
		libr.addRefBook("KomChadLuek",refBook3);
		libr.addRefBook("Java Reference",refBook4);
		libr.addRefBook("Python Reference",refBook5);
		libr.addRefBook("C++ Reference",refBook6);
		
		System.out.println(libr.getBookCount()); //Check amount Book now = 12
		
		libr.borrowBook(stu1, "Marketing");
		System.out.println(libr.getBookCount()); //Check amount Book now = 11
		
		libr.borrowBook(stu1, "PPL");
		System.out.println(libr.getBookCount()); //Check amount Book now = 10
		
		libr.returnBook(stu1, "Marketing");
		System.out.println(libr.getBookCount()); //Check amount Book now = 11
		
		libr.borrowBook(stu2,"C++ Reference");
		System.out.println(libr.getBookCount()); //Check amount Book now = 11 because Reference book can't be borrowed

		libr.borrowBook(stu2, "Korean Language");
		System.out.println(libr.getBookCount()); //Check amount Book now = 10
		
		

		
		

	}

}
